public class MethodsTest {
	public static void main(String[]args)
	{
		int x  = 5;
		System.out.println(x);
		methodNoInputNoReturn();	
		System.out.println(x);
		
		
		
		System.out.println(x);
		 methodOneInputNoReturn(3);
		 System.out.println(x);
		 
		 methodTwoInputNoReturn(2,3.5);
		 methodNoInputReturnInt();
		 int z = methodNoInputReturnInt();
		 System.out.println(z);
		 
		 sumSquareRoot(9,5);
		 System.out.println(sumSquareRoot(9,5));
		 
		 String s1 = "java";
		 String s2 = "programming";
		 System.out.println(s1.length());
		 System.out.println(s2.length());
		 
		 SecondClass.addOne(50);
		 System.out.println(SecondClass.addOne(50));
		 
		 SecondClass sc = new SecondClass();
		 
		 sc.addTwo(50);
		 System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn()
	{
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int youcanCallThisWhateverYouLike)
	{
		System.out.println("Inside the method one input no return");
		youcanCallThisWhateverYouLike = youcanCallThisWhateverYouLike - 5;
		System.out.println(youcanCallThisWhateverYouLike);
	}
	public static void methodTwoInputNoReturn (int num1, double num2)
	{
		System.out.println(num1);
		System.out.println(num2);
	}
	public static int methodNoInputReturnInt()
	{
		return 5;
	}
	public static double sumSquareRoot(int first, int second)
	{
		int sum = first + second;
		double root = Math.sqrt(sum);
		
		return root;
	}
	
}